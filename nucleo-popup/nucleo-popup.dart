import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:Nucleo/providers/app.provider.dart';

class NucleoPopup extends StatelessWidget{
	Widget build(BuildContext context){
		if(context.watch<AppProvider>().popup.visible != false)
			return Container(
				child: Text("Popup")
			);

		return Container(
			width: 0,
			height: 0,
		);
	}
}