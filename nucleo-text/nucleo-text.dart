import 'package:flutter/material.dart';

class NucleoText extends Text{
	TextStyle style;

	NucleoText(
		String data, 
		{
			Key key,
			TextStyle style,
			StrutStyle strutStyle,
			TextAlign textAlign,
			TextDirection textDirection,
			Locale locale,
			bool softWrap,
			TextOverflow overflow,
			double textScaleFactor,
			int maxLines,
			String semanticsLabel,
			TextWidthBasis textWidthBasis,
			TextHeightBehavior textHeightBehavior
		}
	) : 
		this.style = TextStyle(
	        color: Colors.white,
	        decoration: TextDecoration.none,
	        fontSize: 25,
	        fontFamily: "Abolition",
	    ).merge(style),
		super(
			data,
			key: key,
			style: style,
			strutStyle: strutStyle,
			textAlign: textAlign,
			textDirection: textDirection,
			locale: locale,
			softWrap: softWrap,
			overflow: overflow,
			textScaleFactor: textScaleFactor,
			maxLines: maxLines,
			semanticsLabel: semanticsLabel,
			textWidthBasis: textWidthBasis,
			textHeightBehavior: textHeightBehavior,
		);
}